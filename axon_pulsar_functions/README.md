[![Quality Gate Status](http://sonarqube.axon-networks.com/api/project_badges/measure?project=axon%3Apulsar-functions&metric=alert_status)](http://sonarqube.axon-networks.com/dashboard?id=axon%3Apulsar-functions)
[![Security Rating](http://sonarqube.axon-networks.com/api/project_badges/measure?project=axon%3Apulsar-functions&metric=security_rating)](http://sonarqube.axon-networks.com/dashboard?id=axon%3Apulsar-functions)
[![Vulnerabilities](http://sonarqube.axon-networks.com/api/project_badges/measure?project=axon%3Apulsar-functions&metric=vulnerabilities)](http://sonarqube.axon-networks.com/dashboard?id=axon%3Apulsar-functions)

# Pulsar Java Libraries

This directory contains several Java libraries for running on Pulsar. 

## Building

The following tools are required to build:

- Java JDK version 1.8 minimum. You can download the Java environment either through packaging on your platform or via [Oracle](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) directly.

- [Maven](https://maven.apache.org) build system.

Once these requirements are met, you can simply compile the code located in this directory:

`mvn package`

Every directory will have a resulting **target** directory where you will find either the *jar* or *nar* file required for Pulsar.

Each module directory also has a **resource** directory where you will find the associated YAML file for running the Pulsar function.

## Docker Container Memory

Unfortunately, it is a well know fact that Java is a memory pig. To run Pulsar with the current 4 functions, and single IO Sink, the container should have at a minimum 4GB of memory available. The functions and sinks will consume a little over 2GB of memory, \\(>.<)/ 

## Running Pulsar functions

Each Pulsar function located in the directory **axon_pulsar_functions** has a **resource** directory with a YAML file. You can run the functions locally as follows:

`/pulsar/bin/pulsar-admin functions localrun --function-config-file function.yaml`

pulsar-admin does NOT return to the command line, as it is actually loading and running the function. This is different than cluster operations, where the function is started in the cluster. To run several functions at once, you must do the following:

`/pulsar/bin/pulsar-admin functions localrun --function-config-file function.yaml > /dev/null 2>&1 &`

The YAML files currently have some configuration information in them that is specific to ***ahaberstroh's*** setup.

## Running the Pulsar Redis-Hash IO Sink

The *pulsar-io-redis-hash-sink-2.7.0.nar* file must be copied to the following directory: `/pulsar/connectors`.

If for any reason you need to update the .nar file, you must do the following in the docker container:

`rm -rf /tmp/pulsar-nar/`

To run the IO Sink, it is a similar method to running a function, with the same rules with pulsar-admin:

`/pulsar/bin/pulsar-admin sinks localrun --sink-config-file g2c_redis.yaml`

or:

`/pulsar/bin/pulsar-admin sinks localrun --sink-config-file g2c_redis.yaml > /dev/null 2>&1 &`

## Releasing

A Makefile was contributed to make building a releasing easier. 

### Variables

The Makefile has the following variables:

* **VERSION** - This `MUST` match the version in the `POM` file for the version of the release functions. 
* **MIRROR_USER** - You must have a user account on mirror authorized users list in order to push, define that user here. Defaults to root 

### Targets

The Makefile has the following targets:

* **build** - default/ this will just clean and build the functions.
* **package** - this will package up the functions into a tarball
* **push** - this will push a tarball and updates its permission to the mirror server
* **clean** - this will clean the environment. 
* **all** - this will clean, build, package, and push all in one.
* **help** - this will print out a help section. 

### Example

```bash
$ VERSION=1.0.4 MIRROR_USER=monkey make all
```



